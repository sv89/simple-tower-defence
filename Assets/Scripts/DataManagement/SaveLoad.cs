﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class SaveLoad : MonoBehaviour {

	private static SaveLoad instance = null;
	
	protected SaveLoad() { }
	
	public static SaveLoad Instance
	{
		get
		{
			return instance;
		}
	}


	struct battleStats : IComparable<battleStats>
	{
		public int score;
		public int enemiesKilled;
		public int bombsUsed;
		public float timePassed;

		public int CompareTo(battleStats other) {
			return score.CompareTo(other.score) * -1;	//in descending order
		}
	}

	public MainMenuController mainMenuController;
	
	static bool firstTime = true;
	static List<battleStats> stats;

	const int MAX_SAVED = 10;
	
	void Awake(){
		if (SaveLoad.Instance && !(SaveLoad.Instance == this))
			DestroyImmediate (SaveLoad.Instance.gameObject);
		instance = this;
		DontDestroyOnLoad (SaveLoad.instance);

		if (firstTime)
			FirstLoad ();
	}

	void FirstLoad(){
		stats = new List<battleStats> ();
		for (int num = 0; num < MAX_SAVED; num++) {
			if (PlayerPrefs.HasKey ("Score" + num)) {
				stats.Add (new battleStats {
					score = PlayerPrefs.GetInt("Score"+num),
					enemiesKilled = PlayerPrefs.GetInt("Enemies"+num),
					bombsUsed = PlayerPrefs.GetInt("Bombs"+num),
					timePassed = PlayerPrefs.GetFloat("Time"+num),
				});
			};
		}
		firstTime = false;
	}

	void SavePositions(){

		battleStats tempStats;
		for (int num = 0; num < stats.Count; num++) {
			tempStats = stats[num];
			PlayerPrefs.SetInt("Score"+num, tempStats.score);
			PlayerPrefs.SetInt("Enemies"+num, tempStats.enemiesKilled);
			PlayerPrefs.SetInt("Bombs"+num, tempStats.bombsUsed);
			PlayerPrefs.SetFloat("Time"+num, tempStats.timePassed);
		};
	}

	public static void AddNewStats (int score, int enemies, int bombs, float time){
		stats.Add(new battleStats {
			score = score,
			enemiesKilled = enemies,
			bombsUsed = bombs,
			timePassed = time,
		});

		stats.Sort ();

		if (stats.Count > MAX_SAVED)
			stats.RemoveRange (MAX_SAVED - 1, 1);
		else
			stats.TrimExcess ();

		instance.SavePositions ();
	}

	public void StatsOutput(){
		for (int num = 0; num < stats.Count; num++) {
			//Debug.Log("Position " + num + ": score = " + stats[num].score + ", enemies = " + stats[num].enemiesKilled + ", bombs = " + stats[num].bombsUsed + ", time = " + stats[num].timePassed + ".");
			mainMenuController.SetPanelValues(num, stats[num].score, stats[num].enemiesKilled, stats[num].bombsUsed, stats[num].timePassed);
		};
	}

}
