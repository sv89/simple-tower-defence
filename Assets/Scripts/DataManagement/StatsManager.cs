﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StatsManager: MonoBehaviour {

	public static int score;
	public static int enemiesKilled;
	public static int bombsUsed;
	public static float timePassed;
		
	Text text;	
	
	void Awake ()
	{
		text = GetComponent <Text> ();		
		score = 0;
		enemiesKilled = 0;
		bombsUsed = 0;
		timePassed = 0f;
	}
	
	
	void Update ()
	{
		timePassed += Time.deltaTime;
		text.text = "Score: " + score;
	}
}
