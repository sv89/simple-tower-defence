﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenuController : MonoBehaviour {

	public GameObject mainPanel;
	public GameObject statsPanel;
	public Image positionPanelPrefab;
	public Image parentStatsPanel;

	const float PANEL_INIT_Y = 138;
	const float PANEL_OFFSET_Y = 32;
	const int MAX_PANELS = 10;

	Image[] statsPanels;

	public void Awake(){
		CreateStatPanels ();
	}

	public void StartGame(){
		Application.LoadLevel ("level1");
	}

	public void QuitGame() {
		Application.Quit ();
	}

	void CreateStatPanels(){
		statsPanels = new Image[MAX_PANELS];
		for (int i = 0; i < MAX_PANELS; i++) {
			Image panel = Instantiate (positionPanelPrefab) as Image;
			panel.rectTransform.SetParent (parentStatsPanel.rectTransform);
			panel.rectTransform.localPosition = new Vector3 (0f, PANEL_INIT_Y - (PANEL_OFFSET_Y * i), 0f);
			panel.rectTransform.localScale = new Vector3 (1f, 1f, 1f);
			statsPanels[i] = panel;
		}
	}

	public void SetPanelValues(int num, int score, int enemies, int bombs, float time){
		statsPanels[num].GetComponent<PositionPanelController>().SetValues(score, enemies, bombs, time);
	}

}
