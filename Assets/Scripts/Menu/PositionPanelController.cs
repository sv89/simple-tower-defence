﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PositionPanelController : MonoBehaviour {

	public Text scoreTxt;
	public Text enemiesTxt;
	public Text bombsTxt;
	public Text timeTxt;

	public void SetValues( int score, int enemies, int bombs, float time){
		scoreTxt.text = score.ToString();
		enemiesTxt.text = enemies.ToString();
		bombsTxt.text = bombs.ToString();
		timeTxt.text = time.ToString();
	}

}
