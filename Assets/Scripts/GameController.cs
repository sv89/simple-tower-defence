﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	public Canvas hudCanvas;
	public Canvas gameOverCanvas;

	bool isGameOver = false;
	float tempTime;

	void Update(){
		if (isGameOver) {
			Time.timeScale *= 0.9f;
		}
	}

	public void ExitToMenu(){

		if (isGameOver) {
			isGameOver = false;
			Time.timeScale = 1f;
			StatsManager.timePassed = tempTime;
		}

		SaveLoad.AddNewStats (
			StatsManager.score,
			StatsManager.enemiesKilled,
			StatsManager.bombsUsed,
			StatsManager.timePassed
			);

		Application.LoadLevel ("mainmenu");
	}

	public void GameOver(){
		tempTime = StatsManager.timePassed;	//remember correct gameplay time
		isGameOver = true;
		hudCanvas.gameObject.SetActive (false);
		gameOverCanvas.gameObject.SetActive (true);
	}
}
