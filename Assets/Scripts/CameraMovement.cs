﻿using UnityEngine;
using System.Collections;

public class CameraMovement : MonoBehaviour {

	public GameObject parentContainer;
	public float speed = 1.0f;
	public int boundary = 1;

	int width;
	float moveAside;
	float moveForward;
	Vector3 newPos;

	const float XMAX = 40;
	const float XMIN = -40;
	const float ZMAX = 40;
	const float ZMIN = -40;

	void Start () {
		width = Screen.width;
	}
	
	void Update () 
	{
		float moveAside = Input.GetAxis ("Horizontal") * speed * Time.deltaTime;
		float moveForward = Input.GetAxis ("Vertical") * speed * Time.deltaTime;

		parentContainer.transform.Translate (Vector3.right * moveAside, Space.Self);
		parentContainer.transform.Translate (Vector3.forward * moveForward, Space.Self);			

		if (Input.mousePosition.x > width - boundary) {
			parentContainer.transform.Rotate(Vector3.up * Time.deltaTime * speed * 2, Space.World);
		} else if (Input.mousePosition.x < 0 + boundary) {
			parentContainer.transform.Rotate(Vector3.up * Time.deltaTime * speed * -2, Space.World);
		}

		parentContainer.transform.position = new Vector3 (
			Mathf.Clamp(parentContainer.transform.position.x, XMIN, XMAX),
			parentContainer.transform.position.y,
			Mathf.Clamp(parentContainer.transform.position.z, ZMIN, ZMAX)
			);
	}
}
