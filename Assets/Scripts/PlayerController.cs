﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {
	
	public Text healthText;
	public int startingHealth = 20;
	public GameController gameController;

	int health;

	void Start () {
		health = startingHealth;
		UpdateHealthText ();
	}

	void OnTriggerEnter(Collider other){
		if (other.transform.tag.Contains ("Enemy")) {
			health--;
			UpdateHealthText ();
			if (health <= 0)
				gameController.GameOver ();
		}
	}

	void UpdateHealthText(){
		healthText.text = "Health: " + health;
	}
}
