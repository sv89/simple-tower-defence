﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

	public int scoreAmount = 10;
	public float sinkSpeed = 2.5f; 

	GameObject player;
	NavMeshAgent nav;
	Animator anim;
	bool isSinking;

	void Awake(){
		player = GameObject.FindGameObjectWithTag ("Player");
		nav = GetComponent<NavMeshAgent>();
		anim = GetComponent<Animator> ();
	}

	void Start(){
		anim.SetBool("idle0ToRun", true);
	}
	

	void Update () {
		if (isSinking) {
			transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
		} else {
			nav.destination = player.transform.position;
		}
	}

	void OnTriggerEnter(Collider other){

		if (other.tag.Contains ("Bomb")) {

			bool obstacleExists = false;
			float distance = Vector3.Distance (transform.position, other.gameObject.transform.position);
			RaycastHit[] allHits;
			Ray bombRay = new Ray (transform.position, (other.gameObject.transform.position - transform.position));
			allHits = Physics.RaycastAll (bombRay, distance);
			foreach (var hit in allHits) {
				if (hit.collider.tag.Contains ("Terrain") || hit.collider.tag.Contains ("Obstacle"))
					obstacleExists = true;
			}

			if (!obstacleExists)
				Die ( true );	

		} else if (other.tag.Contains ("Player")) {
			Die ( false );	
		}
	}

	void Die( bool isBombed ){
		nav.enabled = false;
		isSinking = true;
		anim.Play ("death");
		Destroy (gameObject, 2f);

		if (isBombed) {
			StatsManager.score += scoreAmount;
			StatsManager.enemiesKilled++;
		}
	}

}
