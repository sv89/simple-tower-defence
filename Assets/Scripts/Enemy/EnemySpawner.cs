﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	public GameObject[] spawnPoints;
	public GameObject[] enemies;
	public int enemyCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;
	public float xRange;
	public float zRange;

	void Start ()
	{
		StartCoroutine (SpawnWaves ());
	}
	
	IEnumerator SpawnWaves ()
	{
		yield return new WaitForSeconds (startWait);
		while (true)
		{
			int point = Random.Range(0, spawnPoints.Length);
			Vector3 spawnPoint = spawnPoints[point].transform.position;
			for (int i = 0; i < enemyCount; i++)
			{
				Vector3 spawnPosition = new Vector3 (
					spawnPoint.x + Random.Range(-xRange, xRange),
					spawnPoint.y,
					spawnPoint.z + Random.Range(-zRange, zRange)
				);
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate (enemies[Random.Range(0,enemies.Length)], spawnPosition, spawnRotation);
				yield return new WaitForSeconds (spawnWait);
			}
			yield return new WaitForSeconds (waveWait);
		}
	}
}
