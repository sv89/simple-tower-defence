﻿using UnityEngine;
using System.Collections;

public class BombController : MonoBehaviour {

	public GameObject parent;	//will used in order to destroy container object
	public float explosionRadius = 10f;
	public float maxLifetime = 0.15f;
	public GameObject explosionPrefab;

	Rigidbody bombRigidbody;
	SphereCollider bombCollider;
	MeshRenderer bombRenderer;

	void Awake(){
		bombRigidbody = GetComponent<Rigidbody> ();
		bombCollider = GetComponent<SphereCollider> ();
		bombRenderer = GetComponent<MeshRenderer> ();
	}

	void OnTriggerEnter(Collider other){
		if (other.tag == "Terrain") {
			bombRigidbody.useGravity = false;
			bombRigidbody.velocity = new Vector3(0f, 0f, 0f);
			bombCollider.radius = explosionRadius;
			bombRenderer.enabled = false;
			Instantiate(explosionPrefab, transform.position, Quaternion.identity);
			Destroy(gameObject, maxLifetime);
			Destroy(parent, maxLifetime);
		}
	}
}
