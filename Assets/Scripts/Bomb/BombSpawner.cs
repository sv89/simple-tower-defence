﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class BombSpawner : MonoBehaviour {

	public Sprite[] bombIco;
	public GameObject[] bombPrefab;

	public Button bombBtn;

	public float bombStartHeight = 10f;
	public float bombSpawnDelay = 1f;

	Ray ray;
	RaycastHit hit;
	Vector3 tempPos;
	Vector3 bombSpawnPos;
	float timeAfterSpawn;
	int currentBomb;

	void Start(){
		currentBomb = 0;
		bombBtn.image.sprite = bombIco[currentBomb];
	}

	void Update () {
		if (timeAfterSpawn > bombSpawnDelay) {
			if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject()){
				ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				Physics.Raycast (ray, out hit);
				if (hit.transform.tag == "Terrain") {
					tempPos = ray.GetPoint(hit.distance);
					bombSpawnPos = new Vector3( tempPos.x, bombStartHeight, tempPos.z );
					Instantiate(bombPrefab[currentBomb], bombSpawnPos, Quaternion.identity);
					timeAfterSpawn = 0f;

					StatsManager.bombsUsed++;
				}
			}
		} else {
			timeAfterSpawn += Time.deltaTime;
		}	
	}

	public void SwitchBomb(){
		currentBomb = (currentBomb < bombPrefab.Length - 1 ? (currentBomb + 1) : 0);
		bombBtn.image.sprite = bombIco[currentBomb];
	}

}
